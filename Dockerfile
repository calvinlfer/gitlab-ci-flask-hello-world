FROM python:3.9-slim

# If you need inter-container communication, this is just providing a hint
EXPOSE 8080

# Install dependencies
RUN pip install micropipenv

# Set up dependencies
COPY Pipfile .
COPY Pipfile.lock .
RUN micropipenv install

# Create a new user and their directory and switch to them
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

# Copy over application
COPY api.py .
COPY static/ static/

# Use a production grade Web Server Gateway Interface (WSGI) to serve the Flask routes
ENTRYPOINT ["waitress-serve", "--port=8080", "--call", "api:create_app"]
